from flask import Flask,request,redirect,url_for,escape,render_template
from flask_cors import CORS
import os
import boto3
import sys
import json
import requests
from moviepy.editor import *
import logging
import tempfile
import urllib
import re
from PIL import Image, ImageDraw, ImageFont
import traceback
import threading
import datetime
import imgkit
import random


app = Flask(__name__)
CORS(app)

bucket="nex-photos"
s3 = boto3.client(
  's3',
  region_name='us-east-2',
  aws_access_key_id='AKIAVCIA3BKNJ7C3KPOS',
  aws_secret_access_key='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'
)
screensize = (1280,720)
error_message="Unable to generate the slideshow file. Please contact the administrator for futher assistance."

def catch_exception(api_endpoint,slideshow_generation_id):
    try:
        exc_info = sys.exc_info()
    finally:
        logging.error(exc_info)
        traceback.print_exception(*exc_info)
        message=str(exc_info) + ' This Error has been occured while creating the slideshow video. Please check the logs in docker container for detailed checking.'
        data={"angular_trace":'',"odoo_trace":message,"current_url":'http://app.nex.photos',"user_id":None,"slideshow_generation_id":slideshow_generation_id}
        logging.error(data)
        if api_endpoint:
            ERROR_API_ENDPOINT = "{0}/web/custom/send_error_traceback_from_aws".format(api_endpoint)
            r = requests.post(url = ERROR_API_ENDPOINT, data = data)
            del exc_info
        logging.error("Error report sent")
        return True

def create_image(content,image,order="front"):
    temp_image=copy_object(image,'.png')
    img = Image.open(temp_image)
    W,H = img.size
    html_template_string=''
    if order == 'front':
        with app.app_context():
            html_template_string=render_template("slider-front.html",
                                        image_url=image,width=W,height=H,
                                        property_name=content.get('property_name'),
                                        full_address=content.get("full_address"),
                                        street2=content.get("street2"))
    else:
        with app.app_context():
            html_template_string=render_template("slider-back.html",
                                        image_url=image,width=W,height=H,
                                        agent_image=content.get('agent_image') or '',
                                        agent_name=content.get('agent_name') or '',
                                        agent_mobile=content.get('agent_mobile') or '',
                                        agent_lic=content.get('agent_lic') or '',
                                        agent_email=content.get('agent_mail') or '',
                                        brokerage_image=content.get('brokerage_image') or '',
                                        brokerage_name=content.get('brokerage_name') or '',
                                        brokerage_lic=content.get('brokerage_lic') or '',
                                        brokerage_street1=content.get('brokerage_street1') or '',
                                        brokerage_street2=content.get('brokerage_street2') or '',
                                        brokerage_city_state_zip=content.get('brokerage_city_state_zip') or '',)
    imgkit.from_string(html_template_string,temp_image)
    return temp_image

def move_video(video_file,filename,agent_name):
    logging.error("move_video")
    s3.upload_file(video_file, bucket, filename)
    location = s3.get_bucket_location(Bucket=bucket)['LocationConstraint']
    s3_url = "https://s3-%s.amazonaws.com/%s/%s" % (location, bucket, filename)
    return s3_url


def copy_object(source_url,extension):
    logging.error(source_url)
    logging.error(extension)
    temp = tempfile.mktemp(suffix=extension)
    if source_url and extension == '.png':
        split_url=source_url.split('/')
        split_url[3]='1280x720'
        source_url='/'.join(split_url)
    response=urllib.request.urlopen(source_url)
    myfile=response.read()
    if myfile:
        with open(temp,'wb') as file:
            file.write(myfile)
    return temp


def create_video(first_image,images,seconds_per_slide,effect_slideshow,last_image):
    image_clips=[]
    padding=0
    position=[('center', 'center'), ('right', 'center'), ('left', 'center'), ('center', 'top'), ('center', 'bottom'), ('left', 'top'), ('right', 'top'), ('left', 'bottom'), ('right', 'bottom')]
    image_clips.append(CompositeVideoClip([ImageClip(first_image).set_duration(5)]))
    for image in images:
        img = Image.open(image)
        size = img.size
        if effect_slideshow =='effect-fade':
            image_clips.append(CompositeVideoClip([ImageClip(image).set_duration(seconds_per_slide).crossfadein(1)]))
            padding=-1
        elif effect_slideshow == 'effect-zoom':
            image_clips.append(CompositeVideoClip([ImageClip(image).resize(size).resize(lambda t: 1 + 0.15*t).set_position(('center', 'center')).set_duration(seconds_per_slide).set_fps(45)]))
            padding=0
        elif effect_slideshow == 'effect-random-zoom-in':
            image_clips.append(CompositeVideoClip([ImageClip(image).resize(size).resize(lambda t: 1 + 0.13*t).set_position(random.choice(position)).set_duration(seconds_per_slide).set_fps(45)]))
            padding=0
        else:
            image_clips.append(CompositeVideoClip([ImageClip(image).set_duration(seconds_per_slide)]))
            padding=0
    image_clips.append(CompositeVideoClip([ImageClip(last_image).set_duration(seconds_per_slide)]))
    video = concatenate(image_clips,method='compose',padding=padding)
    temp = tempfile.mktemp(suffix='.mp4')
    video.write_videofile(temp, fps=50,bitrate="5000k")
    return temp


def images_to_video(content):
    api_endpoint=content.get('api_endpoint')
    slideshow_generation_id=content.get('slideshow_generation_id')
    try:
        temp_files = []
        s3_url=''
        seconds_per_slide=int(content.get('seconds_per_slide')) if content.get('seconds_per_slide') else 7
        effect_slideshow=content.get('effect_slideshow') if content.get('effect_slideshow') else 'effect-fade'

        for image_url in content.get('images')[1:]:
            temp_files.append(copy_object(image_url,'.png'))

        filename=str(content.get('filename'))+'.mp4'
        first_image=create_image(content,content.get('images')[0],'front')
        last_image=create_image(content,random.choice(content.get('images')[:-1]),'last')
        video_file=create_video(first_image,temp_files,seconds_per_slide,effect_slideshow,last_image)

        temp_files.append(video_file)

        if content.get('music_file'):
            music=copy_object(content.get('music_file'), '.mp3')
            temp_music=tempfile.mktemp(suffix='.mp3')
            videoclip = VideoFileClip(video_file)
            music_string="ffmpeg -re -stream_loop -1 -i {0} -t {1} -c copy -y {2}".format(music,videoclip.duration,temp_music)
            os.system(music_string)
            output=tempfile.mktemp(suffix='.mp4')
            string="ffmpeg -i {0} -i {1} -c copy {2} ".format(video_file,temp_music,output)
            os.system(string)
            temp_files.extend([temp_music,output,music])
            video_file=output

        final_output=tempfile.mktemp(suffix='.mp4')
        string='ffmpeg -i {0} -c:v libx264 -strict -2 -preset slow -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" -f mp4 {1}'.format(video_file,final_output)
        os.system(string)
        temp_files.extend([first_image,last_image,final_output])

        folder_path="Tours/Video/{0}".format(filename)
        s3_url=move_video(final_output,folder_path,content.get('agent_name'))

        if temp_files:
            for temp_file in temp_files:
                os.remove(temp_file)
        if s3_url:
            MAIL_API_ENDPOINT= "{0}/web/custom/send_video_mail".format(api_endpoint)
            data={"s3_url":s3_url,"user_id":content.get('user_id'),"property_name":content.get("property_name"),'slideshow_generation_id':slideshow_generation_id}
            r = requests.post(url = MAIL_API_ENDPOINT, data = data)
        logging.error(s3_url)
        logging.error("Completed")
        return s3_url
    except Exception:
        catch_exception(api_endpoint,slideshow_generation_id)
        return json.dumps({'code':204,'message':error_message})


@app.route('/')
def index():
    return json.dumps({'code':200,'message':'Success'})

@app.route('/images_to_video', methods=["POST"])
def process_request():
    content={}
    api_endpoint=''
    slideshow_generation_id=''
    logging.error("Started")

    try:
        if request.method == "POST" and request.is_json and request.get_json():
            content=json.loads(request.get_json())
            logging.error(content)

            if not content:
                return json.dumps({'code':204,'message':error_message})

            if not content.get('images') or not content.get('property_name') or not content.get('slideshow_generation_id') or not content.get('api_endpoint') or not content.get('filename'):
                return json.dumps({'code':204,'message':error_message})

            api_endpoint= content.get('api_endpoint')
            slideshow_generation_id=content.get('slideshow_generation_id')
            video_thread = threading.Thread(target=images_to_video, args=(content,))
            video_thread.start()
            return json.dumps({'code':200,'message':'The slideshow generation is in progress and will be sent to your email after completion. If not please contact the administrator for futher assistance.'})
        else:
            return json.dumps({'code':204,'message':error_message})

    except Exception:
        catch_exception(api_endpoint,slideshow_generation_id)
        return json.dumps({'code':204,'message':error_message})
